Synapse IMAP Auth Provider
==========================

Allows Synapse to use IMAP as a password provider.

This allows users to log in to Synapse with their username and password from an
IMAP/mail account.

Installation
------------

Install with ``./setup.py install``.

Usage
-----

Example Synapse config:

.. code:: yaml

   password_providers:
    - module: "imap_auth_provider.IMAPAuthProvider"
      config:
        host: imap.example.tld
        port: 143
        start_tls: true
        domain: example.tld
        create_users: true

The ``create_users`` key specifies whether to create Matrix accounts for valid
(local parts) of IMAP accounts.

Troubleshooting and Debugging
-----------------------------

``matrix-synapse-imap`` logging is included in the Synapse homeserver log
(typically ``homeserver.log``). The IMAP plugin log level can be increased to
``DEBUG`` for troubleshooting and debugging by making the following modifications
to your Synapse server's logging configuration file:

- Set the value for `handlers.file.level` to `DEBUG`:

.. code:: yaml

   handlers:
     file:
       # [...]
       level: DEBUG

- Add the following to the `loggers` section:

.. code:: yaml

   loggers:
      # [...]
      imap_auth_provider:
        level: DEBUG

Finally, restart your Synapse server for the changes to take effect.
